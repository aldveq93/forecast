import axios from "axios";

class Service {
  constructor(apiKey) {
    this.apiKey = apiKey;
    this.axios = this.createInstance();
  }

  createInstance() {
    const axiosInstance = axios.create({
      baseURL: "https://api.openweathermap.org",
    });

    return axiosInstance;
  }

  async getWeatherDataByLocation(location, appId) {
    try {
      const res = await this.axios.get(
        `/data/2.5/weather?q=${location}&appid=${appId}`
      );
      return res.data;
    } catch (err) {
      return err.message;
    }
  }
}

export default Service;
