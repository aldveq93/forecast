import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./scss/styles.scss";
import ForecastContainer from "./containers/ForecastContainer";

function App() {
  return <ForecastContainer />;
}

export default App;
