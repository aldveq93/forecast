import React, { useState, useEffect } from "react";
import { Form, Button } from "react-bootstrap";
import { openWeatherMapApiKey } from "../../utilities/config";
import Service from "../../services";
import { v4 as uuidv4 } from "uuid";

const SearchField = ({
  setWeatherData,
  setFeedback,
  setLoading,
  setResponseError,
  setLatestSearchingsFromLocalStorage,
}) => {
  const [location, setLocation] = useState("");
  const service = new Service();

  useEffect(() => {
    const getWeatherDataFromApi = async () => {
      // Country / City
      const isLocationEmpty = location === "";
      setFeedback(isLocationEmpty);

      if (isLocationEmpty) {
        return;
      }

      setLoading(true);

      const getWeatherDataByLocation = await service.getWeatherDataByLocation(
        location,
        openWeatherMapApiKey
      );

      if (
        (typeof getWeatherDataByLocation === "string" ||
          getWeatherDataByLocation instanceof String) &&
        getWeatherDataByLocation === "Request failed with status code 404"
      ) {
        setFeedback(false);
        setLoading(false);
        setResponseError(true);
        return;
      }

      const getDestructuredSearchedData = destructDataFromApi(
        getWeatherDataByLocation
      );

      setWeatherDataToLocalStorage(getDestructuredSearchedData);
      setWeatherData(getDestructuredSearchedData);
      setLoading(false);
      setResponseError(false);
    };
    getWeatherDataFromApi();
  }, [location]);

  const destructDataFromApi = (getWeatherDataByLocation) => {
    const {
      name,
      coord: { lat, lon },
      main: { temp, pressure, humidity, temp_max, temp_min },
    } = getWeatherDataByLocation;

    const objFormatted = {
      name,
      temp,
      pressure,
      humidity,
      temp_max,
      temp_min,
      lat,
      lon,
    };

    return objFormatted;
  };

  const setWeatherDataToLocalStorage = (weatherData) => {
    let arraySearchingDataToLocalStorage = [];

    weatherData.id = uuidv4();

    if (existPreviousSearchingsInLocalStorage()) {
      if (isPreviousSearchingsEqualToFive()) {
        updatePreviousSearchings(weatherData);
        return;
      }

      addNewSearchingToLocalStorage(weatherData);
      return;
    }

    arraySearchingDataToLocalStorage.push(weatherData);
    localStorage.setItem(
      "lastestSearches",
      JSON.stringify(arraySearchingDataToLocalStorage)
    );
    setLatestSearchingsFromLocalStorage(
      JSON.parse(localStorage.getItem("lastestSearches"))
    );
  };

  const existPreviousSearchingsInLocalStorage = () => {
    const previousSearchings = JSON.parse(
      localStorage.getItem("lastestSearches")
    );

    if (previousSearchings === null || previousSearchings === undefined) {
      return false;
    } else {
      return true;
    }
  };

  const isPreviousSearchingsEqualToFive = () => {
    const previousSearchings = JSON.parse(
      localStorage.getItem("lastestSearches")
    );

    if (previousSearchings.length === 5) {
      return true;
    } else {
      return false;
    }
  };

  const updatePreviousSearchings = (newWeatherData) => {
    const previousSearchings = JSON.parse(
      localStorage.getItem("lastestSearches")
    );

    previousSearchings.pop();
    previousSearchings.push(newWeatherData);

    localStorage.setItem("lastestSearches", JSON.stringify(previousSearchings));
    setLatestSearchingsFromLocalStorage(previousSearchings);
  };

  const addNewSearchingToLocalStorage = (newWeatherData) => {
    const previousSerchings = JSON.parse(
      localStorage.getItem("lastestSearches")
    );
    previousSerchings.push(newWeatherData);
    localStorage.setItem("lastestSearches", JSON.stringify(previousSerchings));
    setLatestSearchingsFromLocalStorage(previousSerchings);
  };

  const setLocationWithUserInput = (e) => {
    e.preventDefault();
    const inputValue = document.getElementById("searchInput").value;

    if (inputValue === "") {
      alert("Please, provide a location");
      return;
    }

    setLocation(inputValue);
  };

  const clearSearchInput = () => {
    const doc = document;
    doc.getElementById("searchInput").value = "";
    doc.getElementById("searchInput").focus();
    setFeedback(true);
    setLoading(true);
    setResponseError(true);
  };

  return (
    <>
      <div className="mb-3">
        <h1 className="text-white">Welcome to the Forecast App!</h1>
        <p className="text-white">
          Type a country or city to get its weather data &amp; location
        </p>
      </div>

      <Form inline onSubmit={setLocationWithUserInput}>
        <Form.Control
          className="mb-2 mr-sm-2"
          placeholder="Country / City"
          id="searchInput"
        />
        <Button type="submit" className="mb-2">
          Search
        </Button>
        <Button
          type="button"
          variant="secondary"
          className="mb-2 ml-2"
          onClick={clearSearchInput}
        >
          Clear
        </Button>
      </Form>
    </>
  );
};

export default SearchField;
