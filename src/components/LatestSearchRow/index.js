import React from "react";
import { Button } from "react-bootstrap";

const LatestSearchRow = ({
  searchData,
  order,
  setLatestSearchingsFromLocalStorage,
  setFeedback,
  setLoading,
  setResponseError,
  setWeatherData,
}) => {
  const { id, name, temp } = searchData;

  const viewLocationFromLocalStorage = (e) => {
    const target = e.target;
    const locationId = target.getAttribute("data-id");
    const dataSearchingsFromLocalStorage = JSON.parse(
      localStorage.getItem("lastestSearches")
    );

    const searchLocationById = dataSearchingsFromLocalStorage.find(
      (ds) => ds.id === locationId
    );

    setFeedback(false);
    setLoading(false);
    setResponseError(false);
    setWeatherData(searchLocationById);
  };

  const removeLocationFromLocalStorage = (e) => {
    const target = e.target;
    const locationId = target.getAttribute("data-id");
    const dataSearchingsFromLocalStorage = JSON.parse(
      localStorage.getItem("lastestSearches")
    );

    const updatedSearchingsArray = dataSearchingsFromLocalStorage.filter(
      (ds) => ds.id !== locationId
    );

    localStorage.setItem(
      "lastestSearches",
      JSON.stringify(updatedSearchingsArray)
    );
    setLatestSearchingsFromLocalStorage(updatedSearchingsArray);
  };

  return (
    <tr>
      <td className="text-white">{order + 1}</td>
      <td className="text-white">{name}</td>
      <td className="text-white">{temp}º</td>
      <td className="text-white">
        <Button
          type="button"
          variant="primary"
          className="mb-2 ml-2"
          data-id={id}
          onClick={viewLocationFromLocalStorage}
        >
          View
        </Button>
        <Button
          type="button"
          variant="danger"
          className="mb-2 ml-2"
          data-id={id}
          onClick={removeLocationFromLocalStorage}
        >
          Delete
        </Button>
      </td>
    </tr>
  );
};

export default LatestSearchRow;
