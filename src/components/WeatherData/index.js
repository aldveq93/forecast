import React from "react";
import { Card, ListGroup } from "react-bootstrap";
import "./styles.scss";

const WeatherData = ({ weatherData }) => {
  const { name, temp, pressure, humidity, temp_max, temp_min } = weatherData;

  return (
    <>
      <h3 className="text-white mb-4">Weather Data</h3>
      <Card style={{ width: "100%" }}>
        <Card.Header className="weather-data-card-header text-white">
          {name}
        </Card.Header>
        <ListGroup variant="flush">
          <ListGroup.Item>Temperature: {temp}º</ListGroup.Item>
          <ListGroup.Item>Pressure: {pressure}</ListGroup.Item>
          <ListGroup.Item>Humidity: {humidity}</ListGroup.Item>
          <ListGroup.Item>Max Temperature: {temp_max}º</ListGroup.Item>
          <ListGroup.Item>Min Temperature: {temp_min}º</ListGroup.Item>
        </ListGroup>
      </Card>
    </>
  );
};

export default WeatherData;
