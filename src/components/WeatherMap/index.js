import React from "react";
import { Map, Marker, Popup, TileLayer } from "react-leaflet";
import "./styles.scss";

const WeatherMap = ({ weatherData }) => {
  const { name, lat, lon } = weatherData;
  const position = [lat, lon];

  return (
    <>
      <h3 className="text-white mb-4">Location</h3>
      <Map className="leaflet-map-container" center={position} zoom={3}>
        <TileLayer
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        />
        <Marker position={position}>
          <Popup>{name}</Popup>
        </Marker>
      </Map>
    </>
  );
};

export default WeatherMap;
