import React from "react";
import { Table } from "react-bootstrap";
import LatestSearchRow from "../LatestSearchRow";

const LatestSearchings = ({
  latestSearchingsFromLocalStorage,
  setLatestSearchingsFromLocalStorage,
  setFeedback,
  setLoading,
  setResponseError,
  setWeatherData,
}) => {
  return (
    <Table striped bordered hover responsive="sm">
      <thead>
        <tr>
          <th className="text-white">#</th>
          <th className="text-white">Country / City</th>
          <th className="text-white">Temperature</th>
          <th className="text-white">Action</th>
        </tr>
      </thead>
      <tbody>
        {latestSearchingsFromLocalStorage.map((ls, index) => (
          <LatestSearchRow
            key={ls.id}
            order={index}
            searchData={ls}
            setLatestSearchingsFromLocalStorage={
              setLatestSearchingsFromLocalStorage
            }
            setFeedback={setFeedback}
            setLoading={setLoading}
            setResponseError={setResponseError}
            setWeatherData={setWeatherData}
          />
        ))}
      </tbody>
    </Table>
  );
};

export default LatestSearchings;
