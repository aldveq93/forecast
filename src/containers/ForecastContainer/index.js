import React, { useState } from "react";
import { Container, Alert } from "react-bootstrap";
import ForecastValidationContainer from "../ForecastValidationContainer";
import { SearchField, LatestSearchings } from "../../components";

const ForecastContainer = () => {
  const searchesFromLocalStorage = JSON.parse(
    localStorage.getItem("lastestSearches")
  );

  // We can set and || operator to set up the state with useState
  const [
    latestSearchingsFromLocalStorage,
    setLatestSearchingsFromLocalStorage,
  ] = useState(searchesFromLocalStorage || []);
  const [weatherData, setWeatherData] = useState({});
  const [feedback, setFeedback] = useState(true);
  const [loading, setLoading] = useState(false);
  const [responseError, setResponseError] = useState(false);

  return (
    <>
      <Container className="mb-5">
        <SearchField
          setWeatherData={setWeatherData}
          setFeedback={setFeedback}
          setLoading={setLoading}
          setResponseError={setResponseError}
          setLatestSearchingsFromLocalStorage={
            setLatestSearchingsFromLocalStorage
          }
        />
      </Container>

      {feedback ? (
        <Container
          className="d-flex justify-content-center align-items-center"
          style={{ minHeight: "150px" }}
        >
          <Alert variant="warning" style={{ display: "table" }}>
            Try searching a place!
          </Alert>
        </Container>
      ) : (
        <ForecastValidationContainer
          loading={loading}
          responseError={responseError}
          feedback={feedback}
          weatherData={weatherData}
        />
      )}

      {latestSearchingsFromLocalStorage.length > 0 ? (
        <Container>
          <h3 className="text-white mb-4">Latest Searchings</h3>
          <LatestSearchings
            latestSearchingsFromLocalStorage={latestSearchingsFromLocalStorage}
            setLatestSearchingsFromLocalStorage={
              setLatestSearchingsFromLocalStorage
            }
            setFeedback={setFeedback}
            setLoading={setLoading}
            setResponseError={setResponseError}
            setWeatherData={setWeatherData}
          />
        </Container>
      ) : null}
    </>
  );
};

export default ForecastContainer;
