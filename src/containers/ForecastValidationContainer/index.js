import React from "react";
import { Alert, Col, Container, Row } from "react-bootstrap";
import { Loader, WeatherData, WeatherMap } from "../../components";

const ForecastValidationContainer = ({
  loading,
  responseError,
  weatherData,
}) => {
  // Loading state to show a loader
  if (loading) {
    return (
      <Container
        className="d-flex justify-content-center align-items-center"
        style={{ minHeight: "250px" }}
      >
        <Loader />
      </Container>
    );
  }

  //   Return a feedback error if the it did not get data from the api
  if (responseError) {
    return (
      <Container>
        <Alert variant="danger">
          <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
          <p>
            Location not found!
            <br /> Try again!
          </p>
        </Alert>
      </Container>
    );
  }

  //   Default return to render the weather data & map
  return (
    <Container className="mb-5">
      <Row>
        <Col xs={12} lg={6}>
          <WeatherData weatherData={weatherData} />
        </Col>
        <Col xs={12} lg={6}>
          <WeatherMap weatherData={weatherData} />
        </Col>
      </Row>
    </Container>
  );
};

export default ForecastValidationContainer;
